-SAS--Macros
============

Some SAS Macros: 
1. Survival Analysis [9.3] 
   Name       : surv_cat()
   Function   : Number of Total, Number of Death, Median estimated Survival HR and P_value 
   Procedures : lifetest ; phreg
   Other      : including output
   Furture    : multiply variables input
